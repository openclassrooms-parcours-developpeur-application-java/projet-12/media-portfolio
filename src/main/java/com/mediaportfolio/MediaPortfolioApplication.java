package com.mediaportfolio;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class MediaPortfolioApplication {

	public static void main(String[] args) {
		SpringApplication.run(MediaPortfolioApplication.class, args);
	}

}
