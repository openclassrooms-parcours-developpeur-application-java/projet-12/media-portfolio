package com.mediaportfolio.controller;

import com.mediaportfolio.exception.ImageConflictException;
import com.mediaportfolio.exception.ImageNotFoundException;
import com.mediaportfolio.model.ImageEntity;
import com.mediaportfolio.repository.ImageConferenceRepository;
import com.mediaportfolio.repository.ImageRepository;
import com.mediaportfolio.service.dto.ImageDTO;
import com.mediaportfolio.service.mapper.ImageMapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Api(description = "API POUR LES OPERATIONS CRUD SUR LES IMAGES / CONFERENCE")
@RestController
public class ConferenceImageController {

    private static final Logger logger = LoggerFactory.getLogger( ConferenceImageController.class );

    @Autowired
    private ImageConferenceRepository imageConferenceRepository;

    @Autowired
    private ImageRepository imageRepository;
    
    
                                    /* ===================================== */
                                    /* ================ GET ================ */
                                    /* ===================================== */

    /* --------------------------------------- GET IMAGES BY CONFERENCE ---------------------------------------- */
    @ApiOperation(value = "GET ALL IMAGES BY CONFERENCE")
    @GetMapping(value = "/media/image/conference/get-all-image/{conferenceId}")
    public List<ImageDTO> getAllImageByConference(@PathVariable Integer conferenceId) {

        List<ImageDTO> imageDTOList = new ArrayList<>();
        try {
            /**
             *  OBTENIR TOUTES LES IMAGES PAR L'ID CONFERENCE
             * @see ImageConferenceRepository#findAllImageByConferenceId(Integer)
             */
            imageDTOList = ImageMapper.INSTANCE.toDTOList(imageConferenceRepository.findAllImageByConferenceId(conferenceId));
        } catch (Exception pEX) {
            logger.error("{}", String.valueOf(pEX));
        }
        return imageDTOList;
    }


                                        /* ===================================== */
                                        /* ================ ADD ================ */
                                        /* ===================================== */

    /* --------------------------------------- ADD IMAGE FOR CONFERENCE --------------------------------------- */
    @ApiOperation(value = "ADD IMAGE FOR CONFERENCE")
    @PostMapping(value = "/media/image/conference/add-image/{conferenceId}")
    public List<ImageDTO> addImageForConference(@RequestBody List<ImageDTO> imageDTOList,
                                                @PathVariable Integer conferenceId) {

        for (ImageDTO imageDTO : imageDTOList) {
            /**
             * JE VERIFIE SI L'IMAGE EST DEJA PRESENTE DANS LA TABLE
             * @see ImageRepository#findByImagePath(String)
             */
            ImageEntity imageEntity = imageRepository.findByImagePath(imageDTO.getImagePath());

            /**
             * SI L'IMAGE EST DEJA PRESENTE DANS LA TABLE, JE JOINS CELLE-CI A LA TABLE CONFERENCE DANS
             * LA TABLE DE JOINTURE "IMAGE_CONFERENCE", SINON J'AJOUTE LA NOUVELLE IMAGE ET JE CREE LA JOINTURE
             */
            if(imageEntity != null) {

                /** JE VERIFIE SI CONFERENCE ET L'IMAGE SON DEJA LIES
                 * @see ImageConferenceRepository#imageConferenceExists(Integer, Integer)
                 */
                Boolean imageConferenceExists = imageConferenceRepository.imageConferenceExists(imageEntity.getId(), conferenceId);

                if (!imageConferenceExists) {

                    try {

                        /** @see ImageConferenceRepository#findAllImageByConferenceId(Integer) */
                        imageConferenceRepository.joinImageToTheConference(imageEntity.getId(), conferenceId);
                        imageDTO.setId(imageEntity.getId());

                    } catch (DataIntegrityViolationException pEX) {
                        throw new ImageNotFoundException("Conference not found for the ID : " + conferenceId +
                                " - Impossible to link the image to the conference");
                    } catch (Exception pEX) {
                        logger.error(String.valueOf(pEX));
                    }

                } else {
                    throw new ImageConflictException("The image is already linked to the conference : " + conferenceId);
                }

            } else {

                /** JE VERIFIE SI LE CONFERENCE EXISTE
                 * @see ImageConferenceRepository#conferenceExists(Integer)
                 */
                Boolean conferenceExists = imageConferenceRepository.conferenceExists(conferenceId);

                if (conferenceExists) {
                    /** J'AJOUTE L'IMAGE, PUIS JE RECUPERE SON IDENTIFIANT */
                    imageDTO.setId(ImageMapper.INSTANCE.toDTO(
                            imageConferenceRepository.save(ImageMapper.INSTANCE.toEntity(imageDTO))).getId());

                    /** @see ImageConferenceRepository#findAllImageByConferenceId(Integer) */
                    imageConferenceRepository.joinImageToTheConference(imageDTO.getId(), conferenceId);
                } else {
                    throw new ImageNotFoundException("Conference not found for the ID : " + conferenceId +
                            " - Impossible to add the image and link it to the conference");
                }
            }
        }
        return imageDTOList;
    }


                                    /* ======================================= */
                                    /* =============== DELETE  =============== */
                                    /* ======================================= */

    /* --------------------------------------- DEL IMAGE BY CONFERENCE --------------------------------------- */
    @ApiOperation(value = "DEL IMAGE BY CONFERENCE")
    @DeleteMapping(value = "/media/image/conference/del-image/{imageId}/{conferenceId}")
    public String delImageByConference(@PathVariable Integer imageId, @PathVariable Integer conferenceId) {

        String massageStatus = "";

        try {
            /** @see ImageConferenceRepository#deleteImageByConference(Integer, Integer)  */
            Integer check = imageConferenceRepository.deleteImageByConference(imageId, conferenceId);

            if (check == 0) {
                massageStatus = "Failed deletion - image not found";
            } else {
                massageStatus = "Successful deletion of the image";
            }

        } catch (Exception pEX) {
            logger.error(String.valueOf(pEX));
        }
        return massageStatus;
    }
}
