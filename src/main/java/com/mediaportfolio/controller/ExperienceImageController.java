package com.mediaportfolio.controller;

import com.mediaportfolio.exception.ImageConflictException;
import com.mediaportfolio.exception.ImageNotFoundException;
import com.mediaportfolio.model.ImageEntity;
import com.mediaportfolio.repository.ImageExperienceRepository;
import com.mediaportfolio.repository.ImageRepository;
import com.mediaportfolio.service.dto.ImageDTO;
import com.mediaportfolio.service.mapper.ImageMapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.web.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

@Api( description = "API POUR LES OPERATIONS CRUD SUR LES IMAGES / EXPERIENCES" )
@RestController
public class ExperienceImageController {

    private static final Logger logger = LoggerFactory.getLogger( ExperienceImageController.class );

    @Autowired
    private ImageExperienceRepository imageExperienceRepository;
    
    @Autowired
    private ImageRepository imageRepository;


                                    /* ===================================== */
                                    /* ================ GET ================ */
                                    /* ===================================== */

    /* --------------------------------------- GET IMAGES BY EXPERIENCE ---------------------------------------- */
    @ApiOperation(value = "GET ALL IMAGES BY EXPERINCE")
    @GetMapping(value = "/media/image/experience/get-all-image/{experienceId}")
    public List<ImageDTO> getAllImageByExperience(@PathVariable Integer experienceId){

        List<ImageDTO> imageDTOList = new ArrayList<>();

        try {
            /**
             * OBTENIR TOUTES LES IMAGES PAR L'ID EXPERIENCE
             * @see ImageExperienceRepository#findAllImageByExperience(Integer)
             */
            imageDTOList = ImageMapper.INSTANCE.toDTOList(imageExperienceRepository.findAllImageByExperience(experienceId));
        } catch (Exception pEX) {
            logger.error("{}", String.valueOf(pEX));
        }
        return imageDTOList;
    }


                                    /* ===================================== */
                                    /* ================ ADD ================ */
                                    /* ===================================== */

    /* --------------------------------------- ADD IMAGE FOR EXPERINCE --------------------------------------- */
    @ApiOperation(value = "ADD IMAGE FOR EXPERINCE")
    @PostMapping(value = "/media/image/experience/add-image/{experienceId}")
    public List<ImageDTO> addImageForExperience(@RequestBody List<ImageDTO> imageDTOList,
                                                @PathVariable Integer experienceId) {

        for (ImageDTO imageDTO : imageDTOList) {
            /**
             * JE VERIFIE SI L'IMAGE EST DEJA PRESENTE DANS LA TABLE
             * @see ImageRepository#findByImagePath(String)
             */
            ImageEntity imageEntity = imageRepository.findByImagePath(imageDTO.getImagePath());

            /**
             * SI L'IMAGE EST DEJA PRESENTE DANS LA TABLE, JE JOINS CELLE-CI A LA TABLE EXPERINCE DANS
             * LA TABLE DE JOINTURE "IMAGE_EXPERINCE", SINON J'AJOUTE LA NOUVELLE IMAGE ET JE CREE LA JOINTURE
             */
            if(imageEntity != null) {

                /** JE VERIFIE SI EXPERINCE ET IMAGE SON DEJA LIES
                 * @see ImageExperienceRepository#imageExperienceExists(Integer, Integer) 
                 */
                Boolean imageExperienceExists = imageExperienceRepository.imageExperienceExists(imageEntity.getId(), experienceId);
                
                if (!imageExperienceExists) {
                    
                    try {

                        /** @see ImageExperienceRepository#joinImageToTheExperience(Integer, Integer) */
                        imageExperienceRepository.joinImageToTheExperience(imageEntity.getId(), experienceId);
                        imageDTO.setId(imageEntity.getId());
                        
                    } catch (DataIntegrityViolationException pEX) {
                        logger.error(String.valueOf(pEX));
                        throw new ImageNotFoundException("Experience not found for the ID : " + experienceId +
                                " - Impossible to link the image to the experience");
                    } catch (Exception pEX) {
                        logger.error(String.valueOf(pEX));
                    }

                } else {
                    throw new ImageConflictException("The image is already linked to the experience : " + experienceId);
                }
               
            } else {

                /** JE VERIFIE SI EXPERINCE EXISTE
                 * @see ImageExperienceRepository#experienceExists(Integer)
                 */
                Boolean axperienceExists = imageExperienceRepository.experienceExists(experienceId);

                if (axperienceExists) {

                    /** J'AJOUTE L'IMAGE, PUIS JE RECUPERE SON IDENTIFIANT */
                    imageDTO.setId(ImageMapper.INSTANCE.toDTO(
                            imageExperienceRepository.save(ImageMapper.INSTANCE.toEntity(imageDTO))).getId());

                    /** @see ImageExperienceRepository#joinImageToTheExperience(Integer, Integer) */
                    imageExperienceRepository.joinImageToTheExperience(imageDTO.getId(), experienceId);

                } else {
                    throw new ImageNotFoundException("Experience not found for the ID : " + experienceId +
                            " - Impossible to add the image and link it to the experience");
                }
            }
        }
        return imageDTOList;
    }


                                    /* ======================================= */
                                    /* =============== DELETE  =============== */
                                    /* ======================================= */

    /* --------------------------------------- DEL IMAGE BY EXPERINCE --------------------------------------- */
    @ApiOperation(value = "DEL IMAGE BY EXPERINCE")
    @DeleteMapping(value = "/media/image/experience/del-image/{imageId}/{experienceId}")
    public String delImageByExperience(@PathVariable Integer imageId, @PathVariable Integer experienceId) {

        String massageStatus = "";

        try {

            /** @see ImageExperienceRepository#deleteImageByExperience(Integer, Integer) */
            Integer check = imageExperienceRepository.deleteImageByExperience(imageId, experienceId);

            if (check == 0) {
                massageStatus = "Failed deletion - image not found";
            } else {
                massageStatus = "Successful deletion of the image";
            }

        } catch (Exception pEX) {
            logger.error(String.valueOf(pEX));
        }
        return massageStatus;
    }

}
