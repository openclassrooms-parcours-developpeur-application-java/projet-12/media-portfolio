package com.mediaportfolio.controller;

import com.mediaportfolio.exception.ImageConflictException;
import com.mediaportfolio.exception.ImageNotFoundException;
import com.mediaportfolio.repository.ImageRepository;
import com.mediaportfolio.service.dto.ImageDTO;
import com.mediaportfolio.service.mapper.ImageMapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@Api(description = "API POUR LES OPERATIONS SUR LES IMAGES")
@RestController
public class ImageController {

    private static final Logger logger = LoggerFactory.getLogger(ImageController.class);

    @Autowired
    private ImageRepository imageRepository;


                                        /* ===================================== */
                                        /* ============== UPDATE =============== */
                                        /* ===================================== */

    /* ------------------------------------------ UP IMAGE ------------------------------------------ */
    @ApiOperation(value = "UP IMAGE")
    @PutMapping(value = "/media/image/up-image")
    public ImageDTO upImage(@RequestBody ImageDTO imageDTO ) {

        /**
         * JE VERIFIE SI L'IMAGE EXISTE
         */
        Boolean imageExists = imageRepository.existsById(imageDTO.getId());

        if(imageExists) {

            try {
                imageDTO = ImageMapper.INSTANCE.toDTO(imageRepository.save(ImageMapper.INSTANCE.toEntity(imageDTO)));
            } catch (Exception pEX) {
                logger.error("{}", String.valueOf(pEX));
            }

        } else {
            logger.error("Update failure - image : '" + imageDTO.getName() + "' - Is not found");
            throw new ImageNotFoundException("Update failure - image : '" + imageDTO.getName() + "' - Is not found");
        }
        return imageDTO;
    }


                                        /* ======================================= */
                                        /* =============== DELETE  =============== */
                                        /* ======================================= */

    /* ------------------------------------------ DEL IMAGE ------------------------------------------ */
    @ApiOperation(value = "DEL IMAGE")
    @DeleteMapping(value = "/media/image/del-image/{imageId}")
    public String delImage(@PathVariable Integer imageId) {

        String massageStatus = "";

        /**
         * JE VERIFIE SI L'IMAGE EST LIEE AVEC UN PROJET, PROOF OF CONCEPT...
         */
        String imageUseInTheTableProject = imageRepository.imageUseInTheTableProject(imageId);
        String imageUseInTheTableProfOfConcept = imageRepository.imageUseInTheTableProfOfConcept(imageId);
        String imageUseInTheTableParagraph = imageRepository.imageUseInTheTableParagraph(imageId);
        String imageUseInTheTableExperience = imageRepository.imageUseInTheTableExperience(imageId);
        String imageUseInTheTableDiploma = imageRepository.imageUseInTheTableDiploma(imageId);
        String imageUseInTheTableConference = imageRepository.imageUseInTheTableConference(imageId);

        /**
         * SI L'IMAGE EST LIEE, JE RENVOIE UN MESSAGE INDIQUANT LA PROCEDURE A SUIVRE POUR SA SUPPRESSION
         * EST A QUI L'IMAGE EST LIEE
         */
        if (imageUseInTheTableProject != null || imageUseInTheTableProfOfConcept != null
                || imageUseInTheTableParagraph != null || imageUseInTheTableExperience != null
                || imageUseInTheTableDiploma != null || imageUseInTheTableConference != null) {

            if (imageUseInTheTableProject != null)
                throw new ImageConflictException("The image is linked to the project '"
                        + imageUseInTheTableProject + "' - To remove this image you first had to remove the link that links " +
                        "them in the project section.");

            if (imageUseInTheTableProfOfConcept != null)
                throw new ImageConflictException("The image is linked to the proof Of Concept '"
                        + imageUseInTheTableProfOfConcept + "' - To remove this image you first had to remove the link that " +
                        "links them in the proof Of Concept section.");

            if (imageUseInTheTableParagraph != null)
                throw new ImageConflictException("The image is linked to a paragraph of the article'"
                        + imageUseInTheTableParagraph + "' - To remove this image you first had to remove the link that" +
                        " links them in the article section.");

            if (imageUseInTheTableExperience != null)
                throw new ImageConflictException("The image is linked to the experience '"
                        + imageUseInTheTableExperience + "' - To remove this image you first had to remove the link that " +
                        "links them in the experience section.");

            if (imageUseInTheTableDiploma != null)
                throw new ImageConflictException("The image is linked to the diploma '"
                        + imageUseInTheTableDiploma + "' - To remove this image you first had to remove the link that " +
                        "links them in the diploma section.");

            if (imageUseInTheTableConference != null)
                throw new ImageConflictException("The image is linked to the conference '"
                        + imageUseInTheTableConference + "' - To remove this image you first had to remove the link that " +
                        "links them in the conference section.");

        } else {

            try {

                Integer check = imageRepository.deleteImage(imageId);

                if (check == 0) {
                    massageStatus = "Failed deletion - image not found";
                } else {
                    massageStatus = "Successful deletion of the image";
                }

            } catch (Exception pEX) {
                logger.error(String.valueOf(pEX));
            }
        }
        return massageStatus;
    }
}
