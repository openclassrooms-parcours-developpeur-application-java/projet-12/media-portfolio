package com.mediaportfolio.controller;

import com.mediaportfolio.exception.ImageConflictException;
import com.mediaportfolio.exception.ImageNotFoundException;
import com.mediaportfolio.model.ImageEntity;
import com.mediaportfolio.repository.ImageParagraphRepository;
import com.mediaportfolio.repository.ImageProjectRepository;
import com.mediaportfolio.repository.ImageRepository;
import com.mediaportfolio.service.dto.ImageDTO;
import com.mediaportfolio.service.mapper.ImageMapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Api( description = "API POUR LES OPERATIONS CRUD SUR LES IMAGES / PARAGRAPH" )
@RestController
public class ParagraphImageController {

    private static final Logger logger = LoggerFactory.getLogger(ParagraphImageController.class);

    @Autowired
    private ImageParagraphRepository imageParagraphRepository;

    @Autowired
    private ImageRepository imageRepository;


                                        /* ====================================== */
                                        /* ================ GET ================= */
                                        /* ====================================== */

    /* -------------------------------------- GET ALL IMAGES BY PARAGRAPH -------------------------------------- */
    @ApiOperation(value = "GET ALL IMAGES BY ID PARAGRAPH")
    @GetMapping(value = "/media/paragraph/get-all-images/{paragraphId}")
    public List<ImageDTO> getListImageByPragraph(@PathVariable Integer paragraphId) {

        List<ImageDTO> imageDTOList = new ArrayList<>();

        try {
            /**
             * OBTENIR TOUTES LES IMAGES PAR L'ID PARAGRAPH
             * @see ImageParagraphRepository#findAllImageByParagraphId(Integer)
             */
            imageDTOList = ImageMapper.INSTANCE.toDTOList(imageParagraphRepository.findAllImageByParagraphId(paragraphId));
        } catch (Exception pEX) {
            logger.error("{}", String.valueOf(pEX));
        }
        return imageDTOList;
    }


                                        /* ===================================== */
                                        /* ================ ADD ================ */
                                        /* ===================================== */

    /* --------------------------------------- ADD IMAGE FOR PARAGRAPH --------------------------------------- */
    @ApiOperation(value = "ADD IMAGE FOR PARAGRAPH")
    @PostMapping(value = "/media/paragraph/add-image/{paragraphId}")
    public List<ImageDTO> addImageForParagraph(@RequestBody List<ImageDTO> imageDTOList,
                                             @PathVariable Integer paragraphId) {

        for ( ImageDTO imageDTO : imageDTOList ) {

            /**
             * JE VERIFIE SI L'IMAGE EST DEJA PRESENTE DANS LA TABLE
             * @see ImageParagraphRepository#findByImagePath
             * */
            ImageEntity imageEntity = imageRepository.findByImagePath(imageDTO.getImagePath());

            /**
             * SI L'IMAGE EST DEJA PRESENTE DANS LA TABLE, JE JOINS CELLE-CI A LA TABLE PARAGRAPH DANS
             * LA TABLE DE JOINTURE "IMAGE_PARAGRAPH", SINON J'AJOUTE LA NOUVELLE IMAGE ET JE CREE LA JOINTURE
             */
            if (imageEntity != null) {

                /** JE VERIFIE SI LE PARAGRAPH ET L'IMAGE SON DEJA LIES
                 * @see ImageParagraphRepository#imageParagraphExists(Integer, Integer)
                 */
                Boolean imageParagraphExists = imageParagraphRepository.imageParagraphExists(imageEntity.getId(), paragraphId);

                if (!imageParagraphExists) {

                    try {

                        /** @see ImageParagraphRepository#joinImageToTheParagraph(Integer, Integer) */
                        imageParagraphRepository.joinImageToTheParagraph(imageEntity.getId(), paragraphId);
                        imageDTO.setId(imageEntity.getId());

                    } catch (DataIntegrityViolationException pEX) {
                        throw new ImageNotFoundException("Paragraph not found for the ID : " + paragraphId +
                                " - Impossible to link the image to the paragraph");
                    } catch (Exception pEX) {
                        logger.error(String.valueOf(pEX));
                    }

                } else {
                    throw new ImageConflictException("The image is already linked to the paragraph : " + paragraphId);
                }


            } else {
                /** JE VERIFIE SI LE PARAGRAPH EXISTE
                 * @see ImageParagraphRepository#paragraphExists(Integer)
                 */
                Boolean paragraphExists = imageParagraphRepository.paragraphExists(paragraphId);

                if (paragraphExists) {

                    /** J'AJOUTE L'IMAGE, PUIS JE RECUPERE SON IDENTIFIANT */
                    imageDTO.setId(ImageMapper.INSTANCE.toDTO(
                            imageParagraphRepository.save(ImageMapper.INSTANCE.toEntity(imageDTO))).getId());

                    /** @see ImageParagraphRepository#joinImageToTheParagraph(Integer, Integer) */
                    imageParagraphRepository.joinImageToTheParagraph(imageDTO.getId(), paragraphId);

                } else {
                    throw new ImageNotFoundException("Paragraph not found for the ID : " + paragraphId +
                            " - Impossible to add the image and link it to the paragraph");
                }
            }
        }
        return imageDTOList;
    }


                                        /* ======================================= */
                                        /* =============== DELETE  =============== */
                                        /* ======================================= */

    /* --------------------------------------- DEL IMAGE BY PARAGRAPH --------------------------------------- */
    @ApiOperation(value = "DEL IMAGE BY PARAGRAPH")
    @DeleteMapping(value = "/media/paragraph/del-image/{imageId}/{pragraphId}")
    public String delImageByParagraph(@PathVariable Integer imageId, @PathVariable Integer pragraphId) {

        String massageStatus = "";

        try {
            /** @see ImageParagraphRepository#deleteImageByParagraph(Integer, Integer) */
            Integer check = imageParagraphRepository.deleteImageByParagraph(imageId, pragraphId);

            if (check == 0) {
                massageStatus = "Failed deletion - image not found";
            } else {
                massageStatus = "Successful deletion of the image";
            }

        } catch (Exception pEX) {
            logger.error(String.valueOf(pEX));
        }
        return massageStatus;
    }
}
