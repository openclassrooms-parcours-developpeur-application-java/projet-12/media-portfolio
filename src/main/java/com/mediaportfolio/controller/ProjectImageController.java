package com.mediaportfolio.controller;

import com.mediaportfolio.exception.ImageConflictException;
import com.mediaportfolio.exception.ImageNotFoundException;
import com.mediaportfolio.model.ImageEntity;
import com.mediaportfolio.repository.ImageProjectRepository;
import com.mediaportfolio.repository.ImageRepository;
import com.mediaportfolio.service.dto.ImageDTO;
import com.mediaportfolio.service.mapper.ImageMapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.web.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

@Api( description = "API POUR LES OPERATIONS CRUD SUR LES IMAGES / PROJECT" )
@RestController
public class ProjectImageController {

    private static final Logger logger = LoggerFactory.getLogger(ProjectImageController.class);

    @Autowired
    private ImageProjectRepository imageProjectRepository;

    @Autowired
    private ImageRepository imageRepository;


                                    /* ====================================== */
                                    /* ================ GET ================= */
                                    /* ====================================== */

    /* -------------------------------------- GET ALL IMAGES BY PROJECT -------------------------------------- */
    @ApiOperation(value = "GET ALL IMAGES BY PROJECT")
    @GetMapping(value = "/media/project/get-all-image/{projectId}")
    public List<ImageDTO> getAllImageForProject(@PathVariable Integer projectId) {

        List<ImageDTO> imageDTOList = new ArrayList<>();

        try {
            /**
             * OBTENIR TOUTES LES IMAGES PAR L'ID PROJECT
             * @see ImageProjectRepository#findAllImageByProjetcId(Integer)
             */
            imageDTOList = ImageMapper.INSTANCE.toDTOList(imageProjectRepository.findAllImageByProjetcId(projectId));
        } catch (Exception pEX) {
            logger.error("{}", String.valueOf(pEX));
        }
        return imageDTOList;
    }


                                        /* ===================================== */
                                        /* ================ ADD ================ */
                                        /* ===================================== */

    /* --------------------------------------- ADD IMAGE FOR PROJECT --------------------------------------- */
    @ApiOperation(value = "ADD IMAGE FOR PROJECT")
    @PostMapping(value = "/media/project/add-image/{projectId}")
    public List<ImageDTO> addImageForProject(@RequestBody List<ImageDTO> imageDTOList,
                                             @PathVariable Integer projectId) {

        for (ImageDTO imageDTO : imageDTOList) {

            /** JE VERIFIE SI L'IMAGE EST DEJA PRESENTE DANS LA TABLE
             * @see ImageRepository#findByImagePath(String)
             */
            ImageEntity imageEntity = imageRepository.findByImagePath(imageDTO.getImagePath());

            /**
             * SI L'IMAGE EST DEJA PRESENTE DANS LA TABLE, JE JOINS CELLE-CI A LA TABLE PROJECT DANS
             * LA TABLE DE JOINTURE "IMAGE_PROJECT", SINON J'AJOUTE LA NOUVELLE IMAGE ET JE CREE LA JOINTURE
             */
            if (imageEntity != null) {

                /** JE VERIFIE SI LE PROJET ET L'IMAGE SON DEJA LIES
                 * @see ImageProjectRepository#imageProjectExists(Integer, Integer)
                 */
                Boolean imageProjectExists = imageProjectRepository.imageProjectExists(imageEntity.getId(), projectId);

                if(!imageProjectExists) {

                    try {

                        /** @see ImageProjectRepository#joinImagesToTheProject(Integer, Integer) */
                        imageProjectRepository.joinImagesToTheProject(imageEntity.getId(), projectId);
                        imageDTO.setId(imageEntity.getId());

                    } catch (DataIntegrityViolationException pEX) {
                        logger.error(String.valueOf(pEX));
                        throw new ImageNotFoundException("Project not found for the ID : " + projectId +
                                " - Impossible to link the image to the project");
                    } catch (Exception pEX) {
                        logger.error(String.valueOf(pEX));
                    }

                } else {
                    throw new ImageConflictException("The image is already linked to the project : " + projectId);
                }

            } else {
                /** JE VERIFIE SI LE PROJET EXISTE
                 * @see ImageProjectRepository#projectExists(Integer)
                 */
                Boolean projectExists = imageProjectRepository.projectExists(projectId);

                if (projectExists) {

                    /** J'AJOUTE L'IMAGE, PUIS JE RECUPERE SON IDENTIFIANT */
                    imageDTO.setId(ImageMapper.INSTANCE.toDTO(
                            imageProjectRepository.save(ImageMapper.INSTANCE.toEntity(imageDTO))).getId());

                    /** @see ImageProjectRepository#joinImagesToTheProject(Integer, Integer) */
                    imageProjectRepository.joinImagesToTheProject(imageDTO.getId(), projectId);

                } else {
                    throw new ImageNotFoundException("Project not found for the ID : " + projectId +
                            " - Impossible to add the image and link it to the project");
                }
            }
        }
        return imageDTOList;
    }


                                        /* ======================================= */
                                        /* =============== DELETE  =============== */
                                        /* ======================================= */

    /* --------------------------------------- DEL IMAGE BY PROJECT --------------------------------------- */
    @ApiOperation(value = "DEL IMAGE BY PROJECT")
    @DeleteMapping(value = "/media/project/del-image/{imageId}/{projectId}")
    public String delImageByProject(@PathVariable Integer imageId, @PathVariable Integer projectId) {

        String massageStatus = "";

        try {

            /** @see ImageProjectRepository#deleteImageFroProject(Integer, Integer) */
            Integer check = imageProjectRepository.deleteImageFroProject(imageId, projectId);

            if (check == 0) {
                massageStatus = "Failed deletion - image not found";
            } else {
                massageStatus = "Successful deletion of the image";
            }

        } catch (Exception pEX) {
            logger.error(String.valueOf(pEX));
        }
        return massageStatus;
    }
}

