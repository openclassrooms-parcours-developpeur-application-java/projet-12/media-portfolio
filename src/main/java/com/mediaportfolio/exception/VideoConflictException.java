package com.mediaportfolio.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.CONFLICT)
public class VideoConflictException extends RuntimeException {

    public VideoConflictException(String s) {
        super(s);
    }
}
