package com.mediaportfolio.model;

import lombok.*;
import javax.persistence.*;

@Getter @Setter @Builder @AllArgsConstructor @NoArgsConstructor
@Entity @Table( name = "image" )
public class ImageEntity {

    @Id @GeneratedValue
    private Integer id;

    @Column( name = "name" )
    private String name;

    @Column( name = "image_path" )
    private String imagePath;

}
