package com.mediaportfolio.model;

import lombok.*;
import javax.persistence.*;

@Getter @Setter @Builder @AllArgsConstructor @NoArgsConstructor
@Entity @Table( name = "video" )
public class VideoEntity {

    @Id @GeneratedValue
    private Integer id;

    private String title;

    @Column( name = "video_url" )
    private String videoUrl;

    @Column(name = "conference_id")
    private Integer conferenceId;
}
