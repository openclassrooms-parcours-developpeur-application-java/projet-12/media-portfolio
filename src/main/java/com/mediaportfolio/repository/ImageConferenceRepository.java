package com.mediaportfolio.repository;

import com.mediaportfolio.model.ImageEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;

@Repository
public interface ImageConferenceRepository extends JpaRepository<ImageEntity, Integer> {


    /**
     * OBTENIR TOUTES LES IMAGES PAR L'ID CONFERENCE
     * @param conferenceId
     * @return
     */
    @Query(value = "SELECT DISTINCT image.* FROM image" +
            " INNER JOIN image_conference ON image_conference.conference_id= :conferenceId" +
            " WHERE image.id= image_conference.image_id", nativeQuery = true)
    List<ImageEntity> findAllImageByConferenceId(@Param("conferenceId") Integer conferenceId);

    /**
     * VERIFIE SI LE LIEN ENTRE LA TABLE IMAGE ET CONFERENCE EXISTE DEJA
     * @param imageId
     * @param conferenceId
     * @return
     */
    @Query(value = "SELECT count(image_conference) > 0 FROM image_conference" +
            " WHERE image_conference.image_id= :imageId AND image_conference.conference_id= :conferenceId", nativeQuery = true)
    Boolean imageConferenceExists(@Param("imageId") Integer imageId, @Param("conferenceId") Integer conferenceId);

    /**
     * LIER DANS LA TABLE DE JOINTURE IMAGE / CONFERENCE
     * @param imageId
     * @param conferenceId
     */
    @Modifying @Transactional
    @Query(value = "INSERT INTO image_conference (image_id, conference_id)" +
            " VALUES (:imageId, :conferenceId)", nativeQuery = true)
    int joinImageToTheConference(@Param("imageId") Integer imageId, @Param("conferenceId") Integer conferenceId);


    /**
     * SUPPRIMER LA JOINTURE IMAGE / CONFERENCE
     * @param imageId
     * @param conferenceId
     */
    @Modifying @Transactional
    @Query(value = "DELETE FROM image_conference" +
            " WHERE image_id= :imageId AND conference_id= :conferenceId", nativeQuery = true)
    int deleteImageByConference(@Param("imageId") Integer imageId, @Param("conferenceId") Integer conferenceId);

    /**
     * VERIFIE SI LE PROJET EXISTE
     * @param conferenceId
     * @return
     */
    @Query(value = "SELECT COUNT(conference) > 0 FROM conference WHERE conference.id= :conferenceId", nativeQuery = true)
    Boolean conferenceExists(@Param("conferenceId") Integer conferenceId);

}
