package com.mediaportfolio.repository;

import com.mediaportfolio.model.ImageEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;

@Repository
public interface ImageDiplomaRepository extends JpaRepository<ImageEntity, Integer> {


    /**
     * OBTENIR TOUTES LES IMAGES PAR L'ID DIPLOMA
     * @param diplomaId
     * @return
     */
    @Query(value = "SELECT DISTINCT  image.* FROM image" +
            " INNER JOIN image_diploma ON image_diploma.diploma_id= :diplomaId" +
            " WHERE image.id= image_diploma.image_id", nativeQuery = true)
    List<ImageEntity> findAllImageByDiplomaId(@Param("diplomaId") Integer diplomaId);


    /**
     * VERIFIE SI LE LIEN ENTRE LA TABLE IMAGE ET DIPLOMA EXISTE DEJA
     * @param imageId
     * @param diplomaId
     * @return
     */
    @Query(value = "SELECT count(image_diploma) > 0 FROM image_diploma" +
            " WHERE image_diploma.image_id= :imageId AND image_diploma.diploma_id= :diplomaId", nativeQuery = true)
    Boolean imageDiplomaExists(@Param("imageId") Integer imageId, @Param("diplomaId") Integer diplomaId);


    /**
     * LIER DANS LA TABLE DE JOINTURE L'IMAGE ET DIPLOMA
     * @param imageId
     * @param diplomaId
     */
    @Modifying @Transactional
    @Query(value = "INSERT INTO image_diploma (image_id, diploma_id)" +
            " VALUES (:imageId, :diplomaId)", nativeQuery = true)
    int joinImageToTheDiploma(@Param("imageId") Integer imageId, @Param("diplomaId") Integer diplomaId);


    /**
     * SUPPRIMER LA JOINTURE ENTRE UNE IMAGE ET DIPLOMA
     * @param imageId
     * @param diplomaId
     */
    @Modifying @Transactional
    @Query(value = "DELETE FROM image_diploma" +
            " WHERE image_id= :imageId AND diploma_id= :diplomaId", nativeQuery = true)
    int deleteImageByDiploma(@Param("imageId") Integer imageId, @Param("diplomaId") Integer diplomaId);


    /**
     * VERIFIE SI DIPLOMA EXISTE
     * @param diplomaId
     * @return
     */
    @Query(value = "SELECT COUNT(diploma) > 0 FROM diploma WHERE diploma.id= :diplomaId", nativeQuery = true)
    Boolean diplomaExists(@Param("diplomaId") Integer diplomaId);

}
