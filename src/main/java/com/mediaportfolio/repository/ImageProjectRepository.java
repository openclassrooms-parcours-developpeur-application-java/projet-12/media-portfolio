package com.mediaportfolio.repository;

import com.mediaportfolio.model.ImageEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;

@Repository
public interface ImageProjectRepository extends JpaRepository<ImageEntity, Integer> {


    /**
     * OBTENIR TOUTES LES IMAGES PAR L'ID DU PROJET
     * @param projectId
     * @return
     */
    @Query(value = "SELECT DISTINCT  image.* FROM image" +
            " INNER JOIN image_project ON image_project.project_id= :projectId" +
            " WHERE image.id= image_project.image_id", nativeQuery = true)
    List<ImageEntity> findAllImageByProjetcId(@Param("projectId") Integer projectId);


    /**
     * VERIFIE SI LE LIEN ENTRE LA TABLE IMAGE ET PROJECT EXISTE DEJA
     * @param imageId
     * @param projectId
     * @return
     */
    @Query(value = "SELECT count(image_project) > 0 FROM image_project" +
            " WHERE image_project.image_id= :imageId AND image_project.project_id= :projectId", nativeQuery = true)
    Boolean imageProjectExists(@Param("imageId") Integer imageId, @Param("projectId") Integer projectId);


    /**
     * LIER DANS LA TABLE DE JOINTURE L'IMAGE AU PROJET
     * @param imageId
     * @param projectId
     */
    @Modifying @Transactional
    @Query(value = "INSERT INTO image_project (image_id, project_id)" +
            " VALUES (:imageId, :projectId)", nativeQuery = true)
    int joinImagesToTheProject(@Param("imageId") Integer imageId, @Param("projectId") Integer projectId);


    /**
     * SUPPRIMER LA JOINTURE ENTRE UNE IMAGE ET UN PROJET
     * @param imageId
     * @param projectId
     * @return
     */
    @Modifying @Transactional
    @Query(value = "DELETE FROM image_project" +
            " WHERE image_id= :imageId AND project_id= :projectId", nativeQuery = true)
    int deleteImageFroProject(@Param("imageId") Integer imageId, @Param("projectId") Integer projectId);


    /**
     * VERIFIE SI LE PROJET EXISTE
     * @param projectId
     * @return
     */
    @Query(value = "SELECT COUNT(project) > 0 FROM project WHERE project.id= :projectId", nativeQuery = true)
    Boolean projectExists(@Param("projectId") Integer projectId);

}
