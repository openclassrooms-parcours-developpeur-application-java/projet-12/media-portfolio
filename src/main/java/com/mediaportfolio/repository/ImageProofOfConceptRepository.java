package com.mediaportfolio.repository;

import com.mediaportfolio.model.ImageEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;

@Repository
public interface ImageProofOfConceptRepository extends JpaRepository<ImageEntity, Integer> {


    /**
     * OBTENIR TOUTES LES IMAGES PAR L'ID DU PROOF OF CONCEPT
     * @param pocId
     * @return
     */
    @Query(value = "SELECT DISTINCT  image.* FROM image" +
            " INNER JOIN image_proofofconcept ON image_proofofconcept.proof_of_concept_id= :pocId" +
            " WHERE image.id= image_proofofconcept.image_id", nativeQuery = true)
    List<ImageEntity> findAllImageByProofOfConceptId(@Param("pocId") Integer pocId);


    /**
     * VERIFIE SI LE LIEN ENTRE LA TABLE IMAGE ET PROOF OF CONCEPT EXISTE DEJA
     * @param imageId
     * @param pocId
     * @return
     */
    @Query(value = "SELECT count(image_proofofconcept) > 0 FROM image_proofofconcept" +
            " WHERE image_proofofconcept.image_id= :imageId AND image_proofofconcept.proof_of_concept_id= :pocId",
            nativeQuery = true)
    Boolean imageProofOfConceptExists(@Param("imageId") Integer imageId, @Param("pocId") Integer pocId);


    /**
     * LIER DANS LA TABLE DE JOINTURE L'IMAGE AU PROOF OF CONCEPT
     * @param imageId
     * @param pocId
     */
    @Modifying @Transactional
    @Query(value = "INSERT INTO image_proofofconcept (image_id, proof_of_concept_id)" +
            " VALUES (:imageId, :pocId)", nativeQuery = true)
    int joinImagesToTheProofOfConcept(@Param("imageId") Integer imageId, @Param("pocId") Integer pocId);


    /**
     * SUPPRIMER LA JOINTURE ENTRE UNE IMAGE ET UN PROOF OF CONCEPT
     * @param imageId
     * @param pocId
     * @return
     */
    @Modifying @Transactional
    @Query(value = "DELETE FROM image_proofofconcept" +
            " WHERE image_id= :imageId AND proof_of_concept_id= :pocId", nativeQuery = true)
    int deleteImageFroProofOfConcept(@Param("imageId") Integer imageId, @Param("pocId") Integer pocId);


    /**
     * VERIFIE SI LE PROJET EXISTE
     * @param pocId
     * @return
     */
    @Query(value = "SELECT COUNT(proof_of_concept) > 0 FROM proof_of_concept" +
            " WHERE proof_of_concept.id= :pocId", nativeQuery = true)
    Boolean proofOfConceptExists(@Param("pocId") Integer pocId);

}
