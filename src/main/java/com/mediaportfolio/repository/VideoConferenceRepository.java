package com.mediaportfolio.repository;

import com.mediaportfolio.model.VideoEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface VideoConferenceRepository extends JpaRepository<VideoEntity, Integer> {


    /**
     * OBTENIR TOUTES LES VIDEOS PAR L'ID DU CONFERENCE
     * @param conferenceId
     * @return
     */
    List<VideoEntity> findAllByConferenceId(Integer conferenceId);


    /**
     * VERIFIE SI LA VIDEO EXISTE DEJA
     * @param videoUrl
     * @return
     */
    @Query(value = "SELECT COUNT(video) > 0 FROM video WHERE video.video_url= :videoUrl", nativeQuery = true)
    Boolean videoExistsByUrl(@Param("videoUrl") String videoUrl);


    /**
     * SUPPRIMER LA VIDEO PAR SON ID
     * @param videoId
     * @return
     */
    @Modifying @Transactional
    @Query(value = "DELETE FROM video" +
            " WHERE video.id= :videoId", nativeQuery = true)
    int deleteVideoById(@Param("videoId") Integer videoId);

}
