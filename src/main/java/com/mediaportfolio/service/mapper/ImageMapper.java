package com.mediaportfolio.service.mapper;

import com.mediaportfolio.model.ImageEntity;
import com.mediaportfolio.service.dto.ImageDTO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface ImageMapper {

    ImageMapper INSTANCE = Mappers.getMapper( ImageMapper.class );

    ImageDTO toDTO ( ImageEntity imageEntity );

    List<ImageDTO> toDTOList ( List<ImageEntity> imageEntityList );

    ImageEntity toEntity ( ImageDTO imageDTO );

    List<ImageEntity> toEntityList ( List<ImageDTO> imageDTOList );

}
