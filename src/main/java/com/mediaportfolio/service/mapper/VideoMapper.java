package com.mediaportfolio.service.mapper;

import com.mediaportfolio.model.VideoEntity;
import com.mediaportfolio.service.dto.VideoDTO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import java.util.List;

@Mapper
public interface VideoMapper {

    VideoMapper INSTANCE = Mappers.getMapper( VideoMapper.class );

    VideoDTO toDTO (VideoEntity videoEntity );

    List<VideoDTO> toDTOList (List<VideoEntity> videoEntityList );

    VideoEntity toEntity ( VideoDTO videoDTO );

    List<VideoEntity> toEntityList ( List<VideoDTO> videoDTOList );
}
