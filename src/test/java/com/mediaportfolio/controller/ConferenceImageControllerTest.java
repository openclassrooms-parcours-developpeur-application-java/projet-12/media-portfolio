package com.mediaportfolio.controller;

import com.mediaportfolio.exception.ImageConflictException;
import com.mediaportfolio.exception.ImageNotFoundException;
import com.mediaportfolio.model.ImageEntity;
import com.mediaportfolio.repository.ImageConferenceRepository;
import com.mediaportfolio.repository.ImageRepository;
import com.mediaportfolio.service.dto.ImageDTO;
import com.mediaportfolio.service.mapper.ImageMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.dao.DataIntegrityViolationException;
import static org.mockito.ArgumentMatchers.any;
import java.util.ArrayList;
import java.util.List;
import static org.mockito.Mockito.when;

class ConferenceImageControllerTest {

    @Mock
    ImageConferenceRepository imageConferenceRepository;

    @Mock
    ImageRepository imageRepository;

    @InjectMocks
    ConferenceImageController conferenceImageController;

    @BeforeEach
    void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void getAllImageByConference() {

        List<ImageDTO> imageMockList = new ArrayList<>();

        ImageDTO imageMock = ImageDTO.builder().id(1).name("Nom image").imagePath("Path image").build();
        imageMockList.add(imageMock);
        ImageDTO imageMock2 = ImageDTO.builder().id(2).name("Nom image 2").imagePath("Path image 2").build();
        imageMockList.add(imageMock2);

        when(imageConferenceRepository.findAllImageByConferenceId(1))
                .thenReturn(ImageMapper.INSTANCE.toEntityList(imageMockList));

        final List<ImageDTO> imageDTOList = conferenceImageController.getAllImageByConference(1);
        Assertions.assertEquals(imageDTOList.size(), 2);
        Assertions.assertEquals(imageDTOList.get(0).getId(), 1);
        Assertions.assertEquals(imageDTOList.get(1).getId(), 2);
    }

    @Test
    void addImageForConference() {

        List<ImageDTO> imageMockDTOList = new ArrayList<>();
        Integer conferenceIdMock = 1;


        ImageDTO imageDTOMock = ImageDTO.builder().name("Name Mock").imagePath("path_mock").build();
        imageMockDTOList.add(imageDTOMock);
        ImageEntity imageEntityMock = ImageEntity.builder().id(1).name("Name Mock").imagePath("path_mock").build();

        when(imageRepository.findByImagePath(imageDTOMock.getImagePath())).thenReturn(imageEntityMock);
        when(imageConferenceRepository.imageConferenceExists(imageEntityMock.getId(), conferenceIdMock)).thenReturn(false);
        when(imageConferenceRepository.joinImageToTheConference(imageEntityMock.getId(), conferenceIdMock)).thenReturn(1);

        final List<ImageDTO> imageDTOList = conferenceImageController.addImageForConference(imageMockDTOList, conferenceIdMock);
        for (ImageDTO imageDTO : imageDTOList) {
            Assertions.assertEquals(imageDTO.getId(), 1);
        }

        when(imageRepository.findByImagePath(imageDTOMock.getImagePath())).thenReturn(null);
        when(imageConferenceRepository.conferenceExists(conferenceIdMock)).thenReturn(true);
        when(imageConferenceRepository.save(any())).thenReturn(imageEntityMock);
        when(imageConferenceRepository.joinImageToTheConference(imageEntityMock.getId(), conferenceIdMock)).thenReturn(1);

        final List<ImageDTO> imageDTOList1 = conferenceImageController.addImageForConference(imageMockDTOList, conferenceIdMock);
        for (ImageDTO imageDTO : imageDTOList1) {
            Assertions.assertEquals(imageDTO.getId(), 1);
        }
    }

    @Test
    void addImageForConferenceException() {

        List<ImageDTO> imageMockDTOList = new ArrayList<>();
        Integer conferenceIdMock = 1;


        ImageDTO imageDTOMock = ImageDTO.builder().name("Name Mock").imagePath("path_mock").build();
        imageMockDTOList.add(imageDTOMock);
        ImageEntity imageEntityMock = ImageEntity.builder().id(1).name("Name Mock").imagePath("path_mock").build();

        when(imageRepository.findByImagePath(imageDTOMock.getImagePath())).thenReturn(imageEntityMock);
        when(imageConferenceRepository.imageConferenceExists(imageEntityMock.getId(), conferenceIdMock)).thenReturn(true);
        Assertions.assertThrows(ImageConflictException.class, () -> {
            conferenceImageController.addImageForConference(imageMockDTOList, conferenceIdMock);
        });

        when(imageRepository.findByImagePath(imageDTOMock.getImagePath())).thenReturn(imageEntityMock);
        when(imageConferenceRepository.imageConferenceExists(imageEntityMock.getId(), conferenceIdMock)).thenReturn(false);
        when(imageConferenceRepository.joinImageToTheConference(imageEntityMock.getId(), conferenceIdMock))
                .thenThrow(DataIntegrityViolationException.class);
        Assertions.assertThrows(ImageNotFoundException.class, () -> {
            conferenceImageController.addImageForConference(imageMockDTOList, conferenceIdMock);
        });

        when(imageRepository.findByImagePath(imageDTOMock.getImagePath())).thenReturn(null);
        when(imageConferenceRepository.imageConferenceExists(imageEntityMock.getId(), conferenceIdMock)).thenReturn(true);
        when(imageConferenceRepository.conferenceExists(conferenceIdMock)).thenReturn(false);
        Assertions.assertThrows(ImageNotFoundException.class, () -> {
            conferenceImageController.addImageForConference(imageMockDTOList, conferenceIdMock);
        });

    }

    @Test
    void delImageByConference() {

        Integer imageIdMock = 1;
        Integer conferenceIdMock = 5;

        when(imageConferenceRepository.deleteImageByConference(imageIdMock, conferenceIdMock)).thenReturn(1);
        final String checkStatusSuccess = conferenceImageController.delImageByConference(imageIdMock, conferenceIdMock);
        Assertions.assertEquals(checkStatusSuccess, "Successful deletion of the image");

        when(imageConferenceRepository.deleteImageByConference(imageIdMock, conferenceIdMock)).thenReturn(0);
        final String checkStatusFailure = conferenceImageController.delImageByConference(imageIdMock, conferenceIdMock);
        Assertions.assertEquals(checkStatusFailure, "Failed deletion - image not found");
    }
}
