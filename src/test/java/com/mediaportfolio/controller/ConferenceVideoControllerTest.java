package com.mediaportfolio.controller;

import com.mediaportfolio.exception.VideoConflictException;
import com.mediaportfolio.exception.VideoNotFoundException;
import com.mediaportfolio.model.VideoEntity;
import com.mediaportfolio.repository.VideoConferenceRepository;
import com.mediaportfolio.service.dto.VideoDTO;
import com.mediaportfolio.service.mapper.VideoMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import java.util.ArrayList;
import java.util.List;

class ConferenceVideoControllerTest {

    @Mock
    VideoConferenceRepository videoConferenceRepository;

    @InjectMocks
    ConferenceVideoController conferenceVideoController;

    @BeforeEach
    void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void getVideoBy() {

        List<VideoDTO> videoMockDTOList = new ArrayList<>();

        VideoDTO videoMockDTO1 = VideoDTO.builder().id(1).title("Applications web efficaces avec Spring Boot 2")
                .videoUrl("https://www.youtube.com/watch?v=rjtlxiywu3c").conferenceId(1).build();
        videoMockDTOList.add(videoMockDTO1);
        VideoDTO videoMockDTO2 = VideoDTO.builder().id(2).title("Applications web")
                .videoUrl("https://www.youtube.com").conferenceId(1).build();
        videoMockDTOList.add(videoMockDTO2);

        when(videoConferenceRepository.findAllByConferenceId(1))
                .thenReturn(VideoMapper.INSTANCE.toEntityList(videoMockDTOList));

        final List<VideoDTO> videoDTOList  = conferenceVideoController.getVideoByConference(1);
        Assertions.assertEquals(videoDTOList.size(), 2);
        Assertions.assertEquals(videoDTOList.get(0).getId(), 1);
        Assertions.assertEquals(videoDTOList.get(1).getId(), 2);
    }

    @Test
    void addVideoForConference() {

        List<VideoDTO> videoMockDTOList = new ArrayList<>();
        Integer conferenceId = 1;

        VideoDTO videoMockDTO1 = VideoDTO.builder().id(1).title("Applications web efficaces avec Spring Boot 2")
                .videoUrl("https://www.youtube.com/watch?v=rjtlxiywu3c").conferenceId(1).build();
        videoMockDTOList.add(videoMockDTO1);
        VideoEntity videoMockEntity = VideoEntity.builder().id(2).title("Applications web")
                .videoUrl("https://www.youtube.com").conferenceId(1).build();

        when(videoConferenceRepository.videoExistsByUrl(videoMockDTO1.getVideoUrl())).thenReturn(false);
        when(videoConferenceRepository.save(any())).thenReturn(videoMockEntity);
        final List<VideoDTO> videoDTOList = conferenceVideoController.addVideoForConference(videoMockDTOList, conferenceId);
        for (VideoDTO videoDTO : videoDTOList) {
            Assertions.assertEquals(videoDTO.getVideoUrl(), videoMockDTO1.getVideoUrl());
        }
    }

    @Test
    void addVideoForConferenceException() {

        List<VideoDTO> videoMockDTOList = new ArrayList<>();
        Integer conferenceId = 1;

        VideoDTO videoMockDTO1 = VideoDTO.builder().id(1).title("Applications web efficaces avec Spring Boot 2")
                .videoUrl("https://www.youtube.com/watch?v=rjtlxiywu3c").conferenceId(1).build();
        videoMockDTOList.add(videoMockDTO1);
        VideoEntity videoMockEntity = VideoEntity.builder().id(2).title("Applications web")
                .videoUrl("https://www.youtube.com").conferenceId(1).build();

        when(videoConferenceRepository.videoExistsByUrl(videoMockDTO1.getVideoUrl())).thenReturn(true);
        Assertions.assertThrows(VideoConflictException.class, () -> {
            conferenceVideoController.addVideoForConference(videoMockDTOList, conferenceId);
        });
    }

    @Test
    void upVideoByConference() {

        VideoDTO videoMockDTO1 = VideoDTO.builder().id(1).title("Applications web efficaces avec Spring Boot 2")
                .videoUrl("https://www.youtube.com/watch?v=rjtlxiywu3c").conferenceId(1).build();

        when(videoConferenceRepository.existsById(videoMockDTO1.getId())).thenReturn(true);
        when(videoConferenceRepository.save(any())).thenReturn(videoMockDTO1);

        final VideoDTO videoDTO = conferenceVideoController.upVideoByConference(videoMockDTO1);
        Assertions.assertEquals(videoDTO.getId(), videoMockDTO1.getId());

        when(videoConferenceRepository.existsById(videoMockDTO1.getId())).thenReturn(false);
        Assertions.assertThrows(VideoNotFoundException.class, () -> {
            conferenceVideoController.upVideoByConference(videoDTO);
        });
    }

    @Test
    void delVideoByConference() {

        Integer videoIdMock = 1;

        when(videoConferenceRepository.deleteVideoById(videoIdMock)).thenReturn(1);
        final String checkStatusSuccess = conferenceVideoController.delVideoByConference(videoIdMock);
        Assertions.assertEquals(checkStatusSuccess, "Successful deletion of the video");

        when(videoConferenceRepository.deleteVideoById(videoIdMock)).thenReturn(0);
        final String checkStatusFailure = conferenceVideoController.delVideoByConference(videoIdMock);
        Assertions.assertEquals(checkStatusFailure, "Failed deletion - video not found");
    }
}
