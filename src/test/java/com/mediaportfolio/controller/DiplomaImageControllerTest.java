package com.mediaportfolio.controller;

import com.mediaportfolio.exception.ImageConflictException;
import com.mediaportfolio.exception.ImageNotFoundException;
import com.mediaportfolio.model.ImageEntity;
import com.mediaportfolio.repository.ImageDiplomaRepository;
import com.mediaportfolio.repository.ImageRepository;
import com.mediaportfolio.service.dto.ImageDTO;
import com.mediaportfolio.service.mapper.ImageMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.dao.DataIntegrityViolationException;
import java.util.ArrayList;
import java.util.List;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

class DiplomaImageControllerTest {

    @Mock
    ImageDiplomaRepository imageDiplomaRepository;

    @Mock
    ImageRepository imageRepository;

    @InjectMocks
    DiplomaImageController diplomaImageController;

    @BeforeEach
    void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void getAllImageByDiploma() {

        List<ImageDTO> imageMockList = new ArrayList<>();

        ImageDTO imageMock = ImageDTO.builder().id(1).name("Nom image").imagePath("Path image").build();
        imageMockList.add(imageMock);
        ImageDTO imageMock2 = ImageDTO.builder().id(2).name("Nom image 2").imagePath("Path image 2").build();
        imageMockList.add(imageMock2);

        when(imageDiplomaRepository.findAllImageByDiplomaId(1))
                .thenReturn(ImageMapper.INSTANCE.toEntityList(imageMockList));

        final List<ImageDTO> imageDTOList = diplomaImageController.getAllImageByDiploma(1);
        Assertions.assertEquals(imageDTOList.size(), 2);
        Assertions.assertEquals(imageDTOList.get(0).getId(), 1);
        Assertions.assertEquals(imageDTOList.get(1).getId(), 2);
    }

    @Test
    void addImageForDiploma() {

        List<ImageDTO> imageMockDTOList = new ArrayList<>();
        Integer diplomaIdMock = 1;


        ImageDTO imageDTOMock = ImageDTO.builder().name("Name Mock").imagePath("path_mock").build();
        imageMockDTOList.add(imageDTOMock);
        ImageEntity imageEntityMock = ImageEntity.builder().id(1).name("Name Mock").imagePath("path_mock").build();

        when(imageRepository.findByImagePath(imageDTOMock.getImagePath())).thenReturn(imageEntityMock);
        when(imageDiplomaRepository.imageDiplomaExists(imageEntityMock.getId(), diplomaIdMock)).thenReturn(false);
        when(imageDiplomaRepository.joinImageToTheDiploma(imageEntityMock.getId(), diplomaIdMock)).thenReturn(1);

        final List<ImageDTO> imageDTOList = diplomaImageController.addImageForDiploma(imageMockDTOList, diplomaIdMock);
        for (ImageDTO imageDTO : imageDTOList) {
            Assertions.assertEquals(imageDTO.getId(), 1);
        }

        when(imageRepository.findByImagePath(imageDTOMock.getImagePath())).thenReturn(null);
        when(imageDiplomaRepository.diplomaExists(diplomaIdMock)).thenReturn(true);
        when(imageDiplomaRepository.save(any())).thenReturn(imageEntityMock);
        when(imageDiplomaRepository.joinImageToTheDiploma(imageEntityMock.getId(), diplomaIdMock)).thenReturn(1);

        final List<ImageDTO> imageDTOList1 = diplomaImageController.addImageForDiploma(imageMockDTOList, diplomaIdMock);
        for (ImageDTO imageDTO : imageDTOList1) {
            Assertions.assertEquals(imageDTO.getId(), 1);
        }
    }

    @Test
    void addImageForDiplomaException() {

        List<ImageDTO> imageMockDTOList = new ArrayList<>();
        Integer diplomaIdMock = 1;


        ImageDTO imageDTOMock = ImageDTO.builder().name("Name Mock").imagePath("path_mock").build();
        imageMockDTOList.add(imageDTOMock);
        ImageEntity imageEntityMock = ImageEntity.builder().id(1).name("Name Mock").imagePath("path_mock").build();

        when(imageRepository.findByImagePath(imageDTOMock.getImagePath())).thenReturn(imageEntityMock);
        when(imageDiplomaRepository.imageDiplomaExists(imageEntityMock.getId(), diplomaIdMock)).thenReturn(true);
        Assertions.assertThrows(ImageConflictException.class, () -> {
            diplomaImageController.addImageForDiploma(imageMockDTOList, diplomaIdMock);
        });

        when(imageRepository.findByImagePath(imageDTOMock.getImagePath())).thenReturn(imageEntityMock);
        when(imageDiplomaRepository.imageDiplomaExists(imageEntityMock.getId(), diplomaIdMock)).thenReturn(false);
        when(imageDiplomaRepository.joinImageToTheDiploma(imageEntityMock.getId(), diplomaIdMock))
                .thenThrow(DataIntegrityViolationException.class);
        Assertions.assertThrows(ImageNotFoundException.class, () -> {
            diplomaImageController.addImageForDiploma(imageMockDTOList, diplomaIdMock);
        });

        when(imageRepository.findByImagePath(imageDTOMock.getImagePath())).thenReturn(null);
        when(imageDiplomaRepository.imageDiplomaExists(imageEntityMock.getId(), diplomaIdMock)).thenReturn(true);
        when(imageDiplomaRepository.diplomaExists(diplomaIdMock)).thenReturn(false);
        Assertions.assertThrows(ImageNotFoundException.class, () -> {
            diplomaImageController.addImageForDiploma(imageMockDTOList, diplomaIdMock);
        });
    }

    @Test
    void delImageByDiploma() {

        Integer imageIdMock = 1;
        Integer diplomaIdMock = 5;

        when(imageDiplomaRepository.deleteImageByDiploma(imageIdMock, diplomaIdMock)).thenReturn(1);
        final String checkStatusSuccess = diplomaImageController.delImageByDiploma(imageIdMock, diplomaIdMock);
        Assertions.assertEquals(checkStatusSuccess, "Successful deletion of the image");

        when(imageDiplomaRepository.deleteImageByDiploma(imageIdMock, diplomaIdMock)).thenReturn(0);
        final String checkStatusFailure = diplomaImageController.delImageByDiploma(imageIdMock, diplomaIdMock);
        Assertions.assertEquals(checkStatusFailure, "Failed deletion - image not found");
    }
}
