package com.mediaportfolio.controller;

import com.mediaportfolio.exception.ImageConflictException;
import com.mediaportfolio.exception.ImageNotFoundException;
import com.mediaportfolio.repository.ImageRepository;
import com.mediaportfolio.service.dto.ImageDTO;
import com.mediaportfolio.service.mapper.ImageMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

class ImageControllerTest {

    @Mock
    ImageRepository imageRepository;

    @InjectMocks
    ImageController imageController;

    @BeforeEach
    void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void upImage() {

        ImageDTO imageMockDTO = ImageDTO.builder().id(1).name("Nom image").imagePath("Path image").build();

        when(imageRepository.existsById(imageMockDTO.getId())).thenReturn(true);
        when(imageRepository.save(any())).thenReturn(ImageMapper.INSTANCE.toEntity(imageMockDTO));

        final ImageDTO imageDTO = imageController.upImage(imageMockDTO);
        Assertions.assertEquals(imageDTO.getId(), imageMockDTO.getId());

        when(imageRepository.existsById(imageMockDTO.getId())).thenReturn(false);
        Assertions.assertThrows(ImageNotFoundException.class, () -> {
            imageController.upImage(imageMockDTO);
        });
    }

    @Test
    void delImage() {

        Integer imageIdMock = 1;

        when(imageRepository.imageUseInTheTableProject(any())).thenReturn(null);
        when(imageRepository.deleteImage(imageIdMock)).thenReturn(1);
        final String checkStatusSuccess = imageController.delImage(imageIdMock);
        Assertions.assertEquals(checkStatusSuccess, "Successful deletion of the image");

        when(imageRepository.imageUseInTheTableProject(any())).thenReturn(null);
        when(imageRepository.deleteImage(imageIdMock)).thenReturn(0);
        final String checkStatusFailure = imageController.delImage(imageIdMock);
        Assertions.assertEquals(checkStatusFailure, "Failed deletion - image not found");

        when(imageRepository.imageUseInTheTableProfOfConcept(any())).thenReturn("Titre projet");
        Assertions.assertThrows(ImageConflictException.class, () -> {
            imageController.delImage(imageIdMock);
        });
    }
}
