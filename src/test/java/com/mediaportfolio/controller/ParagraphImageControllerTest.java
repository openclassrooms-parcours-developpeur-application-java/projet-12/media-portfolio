package com.mediaportfolio.controller;

import com.mediaportfolio.exception.ImageConflictException;
import com.mediaportfolio.exception.ImageNotFoundException;
import com.mediaportfolio.model.ImageEntity;
import com.mediaportfolio.repository.ImageParagraphRepository;
import com.mediaportfolio.repository.ImageRepository;
import com.mediaportfolio.service.dto.ImageDTO;
import com.mediaportfolio.service.mapper.ImageMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.dao.DataIntegrityViolationException;
import java.util.ArrayList;
import java.util.List;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

class ParagraphImageControllerTest {

    @Mock
    ImageParagraphRepository imageParagraphRepository;

    @Mock
    ImageRepository imageRepository;

    @InjectMocks
    ParagraphImageController paragraphImageController;

    @BeforeEach
    void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void getListImageByArticle() {

        List<ImageDTO> imageMockList = new ArrayList<>();

        ImageDTO imageMock = ImageDTO.builder().id(1).name("Nom image").imagePath("Path image").build();
        imageMockList.add(imageMock);
        ImageDTO imageMock2 = ImageDTO.builder().id(2).name("Nom image 2").imagePath("Path image 2").build();
        imageMockList.add(imageMock2);

        when(imageParagraphRepository.findAllImageByParagraphId(1))
                .thenReturn(ImageMapper.INSTANCE.toEntityList(imageMockList));

        final List<ImageDTO> imageDTOList = paragraphImageController.getListImageByPragraph(1);
        Assertions.assertEquals(imageDTOList.size(), 2);
        Assertions.assertEquals(imageDTOList.get(0).getId(), 1);
        Assertions.assertEquals(imageDTOList.get(1).getId(), 2);
    }

    @Test
    void addImageForArticle() {

        List<ImageDTO> imageMockDTOList = new ArrayList<>();
        Integer paragraphIdMock = 1;

        ImageDTO imageDTOMock = ImageDTO.builder().name("Name Mock").imagePath("path_mock").build();
        imageMockDTOList.add(imageDTOMock);
        ImageEntity imageEntityMock = ImageEntity.builder().id(1).name("Name Mock").imagePath("path_mock").build();

        when(imageRepository.findByImagePath(imageDTOMock.getImagePath())).thenReturn(imageEntityMock);
        when(imageParagraphRepository.imageParagraphExists(imageEntityMock.getId(), paragraphIdMock)).thenReturn(false);
        when(imageParagraphRepository.joinImageToTheParagraph(imageEntityMock.getId(), paragraphIdMock)).thenReturn(1);

        final List<ImageDTO> imageDTOList = paragraphImageController.addImageForParagraph(imageMockDTOList, paragraphIdMock);
        for (ImageDTO imageDTO : imageDTOList) {
            Assertions.assertEquals(imageDTO.getId(), 1);
        }

        when(imageRepository.findByImagePath(imageDTOMock.getImagePath())).thenReturn(null);
        when(imageParagraphRepository.paragraphExists(paragraphIdMock)).thenReturn(true);
        when(imageParagraphRepository.save(any())).thenReturn(imageEntityMock);
        when(imageParagraphRepository.joinImageToTheParagraph(imageEntityMock.getId(), paragraphIdMock)).thenReturn(1);

        final List<ImageDTO> imageDTOList1 = paragraphImageController.addImageForParagraph(imageMockDTOList, paragraphIdMock);
        for (ImageDTO imageDTO : imageDTOList1) {
            Assertions.assertEquals(imageDTO.getId(), 1);
        }
    }

    @Test
    void addImageForArticleException() {

        List<ImageDTO> imageMockDTOList = new ArrayList<>();
        Integer paragraphIdMock = 1;


        ImageDTO imageDTOMock = ImageDTO.builder().name("Name Mock").imagePath("path_mock").build();
        imageMockDTOList.add(imageDTOMock);
        ImageEntity imageEntityMock = ImageEntity.builder().id(1).name("Name Mock").imagePath("path_mock").build();

        when(imageRepository.findByImagePath(imageDTOMock.getImagePath())).thenReturn(imageEntityMock);
        when(imageParagraphRepository.imageParagraphExists(imageEntityMock.getId(), paragraphIdMock)).thenReturn(true);
        Assertions.assertThrows(ImageConflictException.class, () -> {
            paragraphImageController.addImageForParagraph(imageMockDTOList, paragraphIdMock);
        });

        when(imageRepository.findByImagePath(imageDTOMock.getImagePath())).thenReturn(imageEntityMock);
        when(imageParagraphRepository.imageParagraphExists(imageEntityMock.getId(), paragraphIdMock)).thenReturn(false);
        when(imageParagraphRepository.joinImageToTheParagraph(imageEntityMock.getId(), paragraphIdMock))
                .thenThrow(DataIntegrityViolationException.class);
        Assertions.assertThrows(ImageNotFoundException.class, () -> {
            paragraphImageController.addImageForParagraph(imageMockDTOList, paragraphIdMock);
        });

        when(imageRepository.findByImagePath(imageDTOMock.getImagePath())).thenReturn(null);
        when(imageParagraphRepository.imageParagraphExists(imageEntityMock.getId(), paragraphIdMock)).thenReturn(true);
        when(imageParagraphRepository.paragraphExists(paragraphIdMock)).thenReturn(false);
        Assertions.assertThrows(ImageNotFoundException.class, () -> {
            paragraphImageController.addImageForParagraph(imageMockDTOList, paragraphIdMock);
        });
    }

    @Test
    void delImageByParagraph() {

        Integer imageIdMock = 1;
        Integer paragraphIdMock = 5;

        when(imageParagraphRepository.deleteImageByParagraph(imageIdMock, paragraphIdMock)).thenReturn(1);
        final String checkStatusSuccess = paragraphImageController.delImageByParagraph(imageIdMock, paragraphIdMock);
        Assertions.assertEquals(checkStatusSuccess, "Successful deletion of the image");

        when(imageParagraphRepository.deleteImageByParagraph(imageIdMock, paragraphIdMock)).thenReturn(0);
        final String checkStatusFailure = paragraphImageController.delImageByParagraph(imageIdMock, paragraphIdMock);
        Assertions.assertEquals(checkStatusFailure, "Failed deletion - image not found");
    }
}
