package com.mediaportfolio.controller;

import com.mediaportfolio.exception.ImageConflictException;
import com.mediaportfolio.exception.ImageNotFoundException;
import com.mediaportfolio.model.ImageEntity;
import com.mediaportfolio.repository.ImageProofOfConceptRepository;
import com.mediaportfolio.repository.ImageRepository;
import com.mediaportfolio.service.dto.ImageDTO;
import com.mediaportfolio.service.mapper.ImageMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.dao.DataIntegrityViolationException;
import java.util.ArrayList;
import java.util.List;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

class ProofOfConceptImageControllerTest {

    @Mock
    ImageProofOfConceptRepository imageProofOfConceptRepository;

    @Mock
    ImageRepository imageRepository;

    @InjectMocks
    ProofOfConceptImageController proofOfConceptImageController;

    @BeforeEach
    void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void getAllImageByProofOfConcept() {

        List<ImageDTO> imageMockList = new ArrayList<>();

        ImageDTO imageMock = ImageDTO.builder().id(1).name("Nom image").imagePath("Path image").build();
        imageMockList.add(imageMock);
        ImageDTO imageMock2 = ImageDTO.builder().id(2).name("Nom image 2").imagePath("Path image 2").build();
        imageMockList.add(imageMock2);

        when(imageProofOfConceptRepository.findAllImageByProofOfConceptId(1))
                .thenReturn(ImageMapper.INSTANCE.toEntityList(imageMockList));

        final List<ImageDTO> imageDTOList = proofOfConceptImageController.getAllImageByProofOfConcept(1);
        Assertions.assertEquals(imageDTOList.size(), 2);
        Assertions.assertEquals(imageDTOList.get(0).getId(), 1);
        Assertions.assertEquals(imageDTOList.get(1).getId(), 2);
    }

    @Test
    void addImageForProofOfConcept() {
        List<ImageDTO> imageMockDTOList = new ArrayList<>();
        Integer pocIdMock = 1;


        ImageDTO imageDTOMock = ImageDTO.builder().name("Name Mock").imagePath("path_mock").build();
        imageMockDTOList.add(imageDTOMock);
        ImageEntity imageEntityMock = ImageEntity.builder().id(1).name("Name Mock").imagePath("path_mock").build();

        when(imageRepository.findByImagePath(imageDTOMock.getImagePath())).thenReturn(imageEntityMock);
        when(imageProofOfConceptRepository.imageProofOfConceptExists(imageEntityMock.getId(), pocIdMock)).thenReturn(false);
        when(imageProofOfConceptRepository.joinImagesToTheProofOfConcept(imageEntityMock.getId(), pocIdMock)).thenReturn(1);

        final List<ImageDTO> imageDTOList = proofOfConceptImageController.addImageForProofOfConcept(imageMockDTOList, pocIdMock);
        for (ImageDTO imageDTO : imageDTOList) {
            Assertions.assertEquals(imageDTO.getId(), 1);
        }

        when(imageRepository.findByImagePath(imageDTOMock.getImagePath())).thenReturn(null);
        when(imageProofOfConceptRepository.proofOfConceptExists(pocIdMock)).thenReturn(true);
        when(imageProofOfConceptRepository.save(any())).thenReturn(imageEntityMock);
        when(imageProofOfConceptRepository.joinImagesToTheProofOfConcept(imageEntityMock.getId(), pocIdMock)).thenReturn(1);

        final List<ImageDTO> imageDTOList1 = proofOfConceptImageController.addImageForProofOfConcept(imageMockDTOList, pocIdMock);
        for (ImageDTO imageDTO : imageDTOList1) {
            Assertions.assertEquals(imageDTO.getId(), 1);
        }
    }

    @Test
    void addImageForProofOfConceptException() {

        List<ImageDTO> imageMockDTOList = new ArrayList<>();
        Integer pocIdMock = 1;


        ImageDTO imageDTOMock = ImageDTO.builder().name("Name Mock").imagePath("path_mock").build();
        imageMockDTOList.add(imageDTOMock);
        ImageEntity imageEntityMock = ImageEntity.builder().id(1).name("Name Mock").imagePath("path_mock").build();

        when(imageRepository.findByImagePath(imageDTOMock.getImagePath())).thenReturn(imageEntityMock);
        when(imageProofOfConceptRepository.imageProofOfConceptExists(imageEntityMock.getId(), pocIdMock)).thenReturn(true);
        Assertions.assertThrows(ImageConflictException.class, () -> {
            proofOfConceptImageController.addImageForProofOfConcept(imageMockDTOList, pocIdMock);
        });

        when(imageRepository.findByImagePath(imageDTOMock.getImagePath())).thenReturn(imageEntityMock);
        when(imageProofOfConceptRepository.imageProofOfConceptExists(imageEntityMock.getId(), pocIdMock)).thenReturn(false);
        when(imageProofOfConceptRepository.joinImagesToTheProofOfConcept(imageEntityMock.getId(), pocIdMock))
                .thenThrow(DataIntegrityViolationException.class);
        Assertions.assertThrows(ImageNotFoundException.class, () -> {
            proofOfConceptImageController.addImageForProofOfConcept(imageMockDTOList, pocIdMock);
        });

        when(imageRepository.findByImagePath(imageDTOMock.getImagePath())).thenReturn(null);
        when(imageProofOfConceptRepository.imageProofOfConceptExists(imageEntityMock.getId(), pocIdMock)).thenReturn(true);
        when(imageProofOfConceptRepository.proofOfConceptExists(pocIdMock)).thenReturn(false);
        Assertions.assertThrows(ImageNotFoundException.class, () -> {
            proofOfConceptImageController.addImageForProofOfConcept(imageMockDTOList, pocIdMock);
        });
    }

    @Test
    void delImageByProofOfConcept() {

        Integer imageIdMock = 1;
        Integer pocIdMock = 5;

        when(imageProofOfConceptRepository.deleteImageFroProofOfConcept(imageIdMock, pocIdMock)).thenReturn(1);
        final String checkStatusSuccess = proofOfConceptImageController.delImageByProofOfConcept(imageIdMock, pocIdMock);
        Assertions.assertEquals(checkStatusSuccess, "Successful deletion of the image");

        when(imageProofOfConceptRepository.deleteImageFroProofOfConcept(imageIdMock, pocIdMock)).thenReturn(0);
        final String checkStatusFailure = proofOfConceptImageController.delImageByProofOfConcept(imageIdMock, pocIdMock);
        Assertions.assertEquals(checkStatusFailure, "Failed deletion - image not found");
    }

}
